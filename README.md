### Uruchomienie projektu

Biblioteki potrzebne do uruchomienia projektu podane są w pliku `requirements.txt`. Aby uruchomić projekt należy
1. Zainstalować biblioteki podane w `requirements.txt` (np. z użyciem pip):
`pip install -r requirements.txt`
2. Uruchomić projekt poprzez wywołanie pliku w konsoli:
`python huffman.py`