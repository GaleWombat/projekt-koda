import numpy as np
import matplotlib.pyplot as plt
import cv2
import math
import pandas as pd
import heapq
from tqdm import tqdm
from bitarray import bitarray

huffman_lut = {}
inv_huffman_lut = {}
original_double_numbers = {}
used_chars = []

# TODO: dodać zapis LUT


# Klasa odpowiadająca węzłowi w drzewie kodowania Huffmana
class Node:
    def __init__(self, character, probability, right=None, left=None):
        self.character: list = character
        self.probability = probability
        self.right = right
        self.left = left

    def __str__(self):
        return f"{self.character} ({self.probability})"

    def __repr__(self):
        return self.__str__()

    # Funkcje porównujące dodane, aby biblioteka heapq mogła utworzyć stos z instacji klasy Node
    def __eq__(self, other):
        if other == None:
            return False
        if not isinstance(other, Node):
            return False
        return self.probability == other.probability

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):
        return self.probability < other.probability

    def __gt__(self, other):
        return self.probability > other.probability

    def __le__(self, other):
        return (self < other) or (self == other)

    def __ge__(self, other):
        return (self > other) or (self == other)


# utworzenie drzewa z danymi na podstawie kopca
def create_tree(prob):
    nodes = []
    # utworzenie kopca na podstawie prawdopodobieństw występowania symboli
    for k, v in prob.items():
        heapq.heappush(nodes, Node([k], v))
    while len(nodes) > 1:
        right = heapq.heappop(nodes)
        left = heapq.heappop(nodes)
        new_node = Node((right.character + left.character), right.probability + left.probability, right, left)
        heapq.heappush(nodes, new_node)
    return nodes


# Funkcja rekurencyjna odpowiedzialna za przeliczenie kodowania dla danego węzła
def assign_code(root: Node, code_now):
    if root == None:
        return
    if len(root.character) == 1:
        huffman_lut[root.character[0]] = code_now  # mapowanie z symbolu na kod
        inv_huffman_lut[code_now] = root.character[0]
        return
    assign_code(root.left, code_now + "1")
    assign_code(root.right, code_now + "0")


# Funkcja inicjalizująca utworzenie LUT (look up table) dla kodowania i dekowodania kodem Huffmana
def create_codes(nodes):
    root = heapq.heappop(nodes)
    assign_code(root, "")


# TODO: przejść na kodowanie po zakodowanych symbolach ascii zamiast na liczbach:
#  dla modelowania 2 symboli tworzy to problemy np. 8123 to 81 i 23 albo 8 i 123, obecnie rozwiązane za
#  pomocą wstawienia spacji między znaki, ale zwiększa to potencjalnie wielkość słownika do przesłania
# Przeliczanie ilości elementów dla kodowania bez pamięci
def count_elements(data):
    hist_data = {}
    for i in range(len(data)):
        symbol = f"{data[i]}"
        if hist_data.__contains__(symbol):
            hist_data[symbol] += 1
        else:
            hist_data[symbol] = 1
    return hist_data


# Przeliczanie ilości elementów dla kodowania z pamięcią 2
def count_elements_double(data):
    hist_data: dict[str, int] = {}
    for i in range(0, len(data), 2):
        double_base = []
        if i + 1 < len(data):
            symbol = f"{data[i]} {data[i + 1]}"
            double_base.extend([data[i], data[i + 1]])
        else:
            symbol = f"{data[i]}"
            double_base.append(data[i])
        if hist_data.__contains__(symbol):
            hist_data[symbol] += 1
        else:
            hist_data[symbol] = 1
            original_double_numbers[symbol] = double_base
    return hist_data


# Funkcja obliczająca prawdopodobieństwo symboli w zbiorze
def probability(data):
    prob = [x / sum(list(data.values())) for x in list(data.values())]
    keys = list(data.keys())
    return dict(zip(keys, prob))


# Funkcja obliczająca i pokazująca histogram symboli we wprowadzonych danych wejściowych
def histogram(data):
    symbols = list(data)
    df = pd.DataFrame({'symbols': symbols})
    df['num'] = 1
    df = df.groupby('symbols').sum().sort_values('num', ascending=False) / len(df)
    plt.title("Entry symbols histogram")
    plt.xlabel('Symbol (as number)')
    plt.ylabel('Probability')  # ?
    plt.bar(df.index, df.num, width=0.5, color='grey')
    plt.show()


# Funkcja obliczająca entropię na podstawie podanych prawdopodobieństw występowania symboli
def entropy(prob):
    entr = 0
    val = list(prob.values())
    sum_values = sum(prob.values())
    for i in val:
        entr += -(i / sum_values * math.log(i, 2))
    return entr


def average_length(data):
    # TODO: naprawić
    avg = 0
    dict = probability(count_elements(data))
    for i in list(dict.items()):
        avg += i[1] * len(i[0])
    return avg


# Funkcja odpowiadająca za zarządzanie kodowaniem Huffmana
# TODO: przejść na kodowanie do bitarray żeby maksymalnie zmniejszyć słowa kodowa
def coder(input_data_path, output_data_path, model=None):
    if model == 'model1':
        with open(input_data_path, 'rb+') as file, open(output_data_path, 'wb') as output:
            data = file.read()
            prob = probability(count_elements(data))
            histogram(data)
            plt.show()
            print('The entropy: ', entropy(prob))
            nodes = create_tree(prob)
            create_codes(nodes)
            out_data = []
            for i in data:
                out_data.extend(huffman_lut[str(i)])
            if len(set(huffman_lut.values())) != len(huffman_lut.values()):
                raise ValueError("Incorrect Huffman encoding: some codes are ambiguous!")
            output.write("".join(out_data).encode())
            return out_data
    elif model == 'model2':
        with open(input_data_path, 'rb+') as file, open(output_data_path, 'wb') as output:
            data = file.read()
            histogram(data)
            plt.show()
            prob = probability(count_elements_double(data))
            print('The entropy: ', entropy(prob))
            nodes = create_tree(prob)
            create_codes(nodes)
            out_data = []
            for i in range(0, len(data), 2):
                if i + 1 < len(data):
                    out_data.extend(huffman_lut[f"{data[i]} {data[i + 1]}"])
                else:
                    out_data.extend(huffman_lut[f"{data[i]}"])
            if len(set(huffman_lut.values())) != len(huffman_lut.values()):
                raise ValueError("Incorrect Huffman encoding: some codes are ambiguous!")
            output.write("".join(out_data).encode())
            return out_data
    elif model == 'model3':
        # TODO: implementacja Markova
        print('not implemented yet')
    else:
        print('not implemented yet')


# Funkcja odpowiadająca za zarządzanie dekodowanie kodu Huffmana
def decoder(input_data_path, output_data_path, model=None):
    with open(input_data_path, "rb+") as input, open(output_data_path, 'wb') as output:
        data = input.read()
        out_data = []
        buff = []
        for d in tqdm(data):
            buff.extend(chr(d))
            if inv_huffman_lut.__contains__(''.join(buff)):
                if model == 'model2':
                    out_data.extend(original_double_numbers[inv_huffman_lut[''.join(buff)]])
                else:
                    out_data.append(int(inv_huffman_lut[''.join(buff)]))
                buff.clear()
        output.write(bytearray(out_data))


model = 'model2'
# output_data = coder('rozklady/uniform.pgm', 'uniform_coded.txt', model)
output_data = coder('obrazy/boat.pgm', 'boat_coded.txt', model)
print('Staring decoding')
# decoder('uniform_coded.txt', 'uniform_decoded.pgm', model)
decoder('boat_coded.txt', 'boat_decoded.pgm', model)
print('Decoding finished')
print('The average codeword length: ', average_length(output_data))


def compare_images(image_1_path, image_2_path):
    with open(image_1_path, 'rb+') as im1, open(image_2_path, 'rb+') as im2:
        data1 = im1.read()
        data2 = im2.read()
        diff_count = 0
        print(f"Lenghts of files: 1 - {len(data1)} | 2 - {len(data2)}")
        for i in range(max(len(data1), len(data2))):
            if i < len(data1) and i < len(data2):
                if data1[i] != data2[i]:
                    diff_count += 1
                    print(f"diff on pixel {i}: 1 - {data1[i]} | 2 - {data2[i]}")
            elif i < len(data1):
                diff_count += 1
                print(f"diff on pixel {i}: 1 - {data1[i]} | 2 - NO DATA")
            elif i < len(data2):
                diff_count += 1
                print(f"diff on pixel {i}: 1 - NO DATA | 2 - {data2[i]}")
        print(f"Diff counter: {diff_count}")


# compare_images('rozklady/uniform.pgm', 'uniform_decoded.pgm')
compare_images('obrazy/boat.pgm', 'boat_decoded.pgm')
